==============
Django-PNotify
==============

Frontend
--------

Creating a notification in JS (for frontend-related notices): ::

    options = {
        title: "Oh no!",
        text: "Something terrible happened.",
        type: "error"
    }
    create_notification(get_pnotify_options(options));

Function ``get_pnotify_options()`` translates given object (typically coming from backend) to a PNotify-compatible options object.

Backend
-------

**Flash messages**

To display a message only once **on the next page refresh** (which makes it useless for ajax actions), simply create a standard Django message. ::

    from django.contrib import messages
    messages.error(request, 'Some serious error occured')
    messages.success(request, 'We won!')

**Custom notification**

This module allows you to create your own classes of notifications, which define appearance and behaviour of notification. Such notifications can contain buttons and/or prompt and register callbacks (i.e. backend functions which notification form is being submitted to). A simple dialog box with a prompt looks like this: ::

    # dummy_app/notifications.py
    from pnotify.notifications import AbstractConfirmNotification

    class DummyNotification(AbstractConfirmNotification):
        title = "Testing notification"
        text = "This is a testing message.\nAnd its new line."
        prompt = True

        def callback(self, request):
            self.bound_obj.delete()
            prompt = request.POST.get('prompt')
            if request.POST.get('button') == 'ok':
                return DummySuccessNotification(prompt)
            else:
                return DummyErrorNotification()
        class Meta:
            name = 'dummy'

First of all, this class extends ``AbstractConfirmNotification`` class, which is an abstract empty dialog box with two buttons: ``OK`` and ``Cancel``. ::

    # in pnotify/notifications.py
    from buttons import OKButton, CancelButton

    class AbstractConfirmNotification(AbstractNotification):
        """
        Base class for all notifications with "OK" and "Cancel" buttons.
        """
        buttons = (
            OKButton(),
            CancelButton(),
        )
        hide = False
        hide_button = True

        class Meta:
            abstract = True

Then it defines title and body text of the notification. It also sets ``prompt`` flag to ``True``, which means that the notification should contain an input field. Next member is a ``callback(request)`` function which is called by the user on button click (``OK`` or ``Cancel``). It first deletes ``self.bound_obj`` which is a ``messaging.models.Message`` object that stores information about the notification in database, so the notification won't be displayed again. Then it parses the input, which is two POST args:

- ``button`` - tells server which button has been clicked by user
- ``prompt`` - user input from prompt

``callback(request)`` function can return another notification object, that will be nicely replace the old one.

There's also Django-like ``class Meta`` subclass within ``DummyNotification``. It may contain one of two possible members:

- ``abstract`` - specifies an abstract class (i.e. you cannot send notifications of such class)
- ``name`` - short name of the notification type that will be used to store info about the notification in database (if empty, class name is used)

To get to know all options of a notification, take a look at ``AbstractNotification`` class in ``messaging/notifications.py`` file.

When you have your notification class, you can start sending notifications to users as simple as that: ::

    from pnotify.models import Message

    Message.create('dummy', receiver=request.user)

