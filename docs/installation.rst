============
Installation
============

At the command line::

    $ easy_install django-pnotify

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-pnotify
    $ pip install django-pnotify