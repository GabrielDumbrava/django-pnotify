class BaseButton(object):
    id = ''

    def get_id(self):
        """ Notification id. """
        return self.id

    label = ''

    def get_label(self):
        """ Notification label. """
        return self.label

    type = 'default'

    def get_type(self):
        """ Notification type. """
        return self.type

    prompt_trigger = 'default'

    def get_prompt_trigger(self):
        """ Notification prompt_trigger. """
        return self.prompt_trigger

    def __init__(self, prompt_trigger=False):
        """
        @param prompt_trigger: If user hits enter while typing in a prompt, button with prompt_trigger set to True gets clicked.
        """
        self.prompt_trigger = prompt_trigger

    def serialize(self):
        """
        Serializes button to a dictionary that (after translating to JSON)
        will be parsed by the front-end.
        """
        res = {
            'id': self.get_id(),
            'label': self.get_label(),
            'type': self.get_type(),
            'prompt_trigger': self.get_prompt_trigger(),
        }
        return res


class OKButton(BaseButton):
    label = 'OK'
    type = 'success'

    def __init__(self, id_param='ok'):
        self.id = id_param
        super(OKButton, self).__init__(prompt_trigger=True)


class CancelButton(BaseButton):
    label = 'Cancel'

    def __init__(self, id_param='cancel'):
        self.id = id_param
        super(CancelButton, self).__init__()