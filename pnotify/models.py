# -*- coding: utf-8 -*-
from pnotify import logger
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from jsonfield import JSONField


class NotificationContainer(object):
    registered_notifications = dict()

    @classmethod
    def register_notification_class(cls, f, name):
        if name in cls.registered_notifications:
            logger.debug('Notification class `%s` has been registered twice.' % name)
        cls.registered_notifications[name] = f

    @classmethod
    def get_notification(cls, name, obj):
        if name not in cls.registered_notifications:
            raise KeyError('Notification class `%s` not found.' % name)
        return cls.registered_notifications[name](obj)


class MessageManager(models.Manager):
    def inbox(self, user):
        return self.filter(receiver=user).all()

    def outbox(self, user):
        return self.filter(sender=user).all()


class Message(models.Model):
    sender = models.ForeignKey(User, related_name='messages_sent', blank=True, null=True)
    receiver = models.ForeignKey(User, related_name='messages_received')
    notification_type = models.CharField(max_length=128)
    notification_options = JSONField(default={}, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = MessageManager()

    class Meta(object):
        ordering = ('created_at', )

    @classmethod
    def create(cls, notification_type, receiver, notification_options={}):
        msg = Message()
        msg.notification_type = notification_type
        msg.notification_options = notification_options
        msg.receiver = receiver
        return msg

    def serialize(self):
        serialized = {
            'id': self.pk,
            'created_at': int(self.created_at.strftime('%s')),
            'updated_at': int(self.updated_at.strftime('%s')),
            'callback_url': reverse('notification_callback', args=(self.pk,)),
        }
        return serialized

    def notification(self):
        return NotificationContainer.get_notification(self.notification_type, self)