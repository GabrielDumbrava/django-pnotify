from django.contrib import messages as FlashMessages
from .models import NotificationContainer, Message
from .buttons import OKButton, CancelButton


class AbstractNotification(object):
    """
    Base class for all notification classes.
    """

    class __metaclass__(type):
        def __init__(cls, name, bases, dict):
            type.__init__(cls, name, bases, dict)
            # Process 'Meta' class
            if 'Meta' in dict:
                if hasattr(cls.Meta, 'abstract') and cls.Meta.abstract:
                    return
                if hasattr(cls.Meta, 'name'):
                    name = cls.Meta.name
            NotificationContainer.register_notification_class(cls, name)

    class Meta:
        abstract = True

    title = ''

    def get_title(self):
        """ Notification title. """
        return self.title

    text = ''

    def get_text(self):
        """ Notification text. """
        return self.text

    icon = ''

    def get_icon(self):
        """ Notification icon that overrides default bootstrap icons. """
        return self.icon

    buttons = ()

    def get_buttons(self):
        """ List or tuple of buttons to be embedded in the notification. """
        return self.buttons

    prompt = False

    def get_prompt(self):
        """ Determines whether to prompt for user input. """
        return self.prompt

    prompt_multi_line = False

    def get_prompt_multi_line(self):
        """ Determines whether to use <input type="text"> or <textarea>. """
        return self.prompt_multi_line

    prompt_default = ''

    def get_prompt_default(self):
        """ Default prompt value. """
        return self.prompt_default

    stack = 'default'

    def get_stack(self):
        """ Stack to which a notification should be appended. """
        return self.stack

    additional_class = ''

    def get_additional_class(self):
        """ Class added to the notification DOM object. """
        return self.additional_class

    hide = False

    def get_hide(self):
        """ Determines whether to hide the notification. """
        return self.hide

    hide_button = True

    def get_hide_button(self):
        """ Determines whether to show a hide button. """
        return self.hide_button

    type = 'notice'

    def get_type(self):
        """ Type of the notice: 'notice', 'info', 'success', or 'error'. """
        return self.type

    bound_obj = None

    def __init__(self, obj=None):
        """
        @param obj: Message object bounded to this notification class.
        """
        self.bound_obj = obj

    def callback(self, request):
        """
        Callback function called when user clicks a button.
        """
        self.bound_obj.delete()

    def post_display(self):
        """
        A function being called after a notification is being displayed to receiver.
        E.g. you could delete delete it from database.
        """
        pass

    def serialize(self, request):
        """
        Serializes notification to a dictionary that (after translating to JSON)
        will be parsed by the front-end.
        """
        res = {
            'title': self.get_title(),
            'text': self.get_text(),
            'icon': self.get_icon(),
            'buttons': [b.serialize() for b in self.get_buttons()],
            'prompt': self.get_prompt(),
            'prompt_multi_line': self.get_prompt_multi_line(),
            'prompt_default': self.get_prompt_default(),
            'stack': self.get_stack(),
            'additional_class': self.get_additional_class(),
            'hide': self.get_hide(),
            'hide_button': self.get_hide_button(),
            'type': self.get_type(),
        }
        if hasattr(self.bound_obj, 'serialize'):
            res.update(self.bound_obj.serialize())
        return res


class AbstractConfirmNotification(AbstractNotification):
    """
    Base class for all notifications with "OK" and "Cancel" buttons.
    """
    buttons = (
        OKButton(),
        CancelButton(),
    )
    hide = False
    hide_button = True

    class Meta:
        abstract = True


class FlashMessageNotification(AbstractNotification):
    """
    Notification class for flash messages (i.e. Django messages framework).
    """

    def get_title(self):
        if self.bound_obj.level == FlashMessages.DEBUG:
            title = "Debug"
        elif self.bound_obj.level == FlashMessages.INFO:
            title = "Notice"
        elif self.bound_obj.level == FlashMessages.SUCCESS:
            title = "Success"
        elif self.bound_obj.level == FlashMessages.WARNING:
            title = "Warning"
        elif self.bound_obj.level == FlashMessages.ERROR:
            title = "Error"
        return title

    def get_text(self):
        return self.bound_obj.message

    def get_type(self):
        if self.bound_obj.level == FlashMessages.DEBUG:
            type = "default"
        elif self.bound_obj.level == FlashMessages.INFO:
            type = "info"
        elif self.bound_obj.level == FlashMessages.SUCCESS:
            type = "success"
        elif self.bound_obj.level == FlashMessages.WARNING:
            type = "default"
        elif self.bound_obj.level == FlashMessages.ERROR:
            type = "error"
        return type


class DummySuccessNotification(AbstractNotification):
    title = "Woo hoo!"
    text = "It worked! You typed in \"%s\""
    type = "success"

    def get_text(self):
        return self.text % self.input

    def __init__(self, input):
        self.input = input


class DummyErrorNotification(AbstractNotification):
    title = "Boo hoo!"
    text = "You canceled."
    type = "error"


class DummyUnforgivingNotification(AbstractNotification):
    title = "Boo!"
    text = "I still can't believe you actually cancelled.\nBy the way, you typed in \"%s\""
    type = "error"

    def post_display(self):
        # Display it only once. We could as well use simple Django messaging for this.
        self.bound_obj.delete()

    def get_text(self):
        return self.text % self.bound_obj.notification_options['msg']

    class Meta:
        name = 'dummy_unforgiving_notification'


class DummyNotification(AbstractConfirmNotification):
    """
    Testing notification. Simply prompts for an input with two buttons - ok and cancel.
    """

    title = "Testing notification"
    text = "This is a testing message.\nAnd its new line."
    prompt = True

    def callback(self, request):
        self.bound_obj.delete()
        prompt = request.POST.get('prompt')
        if request.POST.get('button') == 'ok':
            return DummySuccessNotification(prompt)
        else:
            message = Message.create('dummy_unforgiving_notification', request.user, dict(msg=prompt))
            message.save()
            # in that case, pretty much the same as:
            # from django.contrib import messages
            # messages.error(request, 'I still can\'t believe you actually cancelled.\nBy the way, you typed in "%s"' % prompt)
            return DummyErrorNotification()

    class Meta:
        name = 'dummy'