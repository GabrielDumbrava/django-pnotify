var loading_notification;
var server_error_notification;

var stacks = {
	'bottomright': {'dir1': 'left', 'dir2': 'up', 'push': 'top', 'class': 'stack-bottomright'},
	'bottomleft': {'dir1': 'right', 'dir2': 'up', 'push': 'top', 'class': 'stack-bottomleft'},
	'topright': {'dir1': 'down', 'dir2': 'left', 'push': 'top', 'class': 'stack-topright'},
	'topleft': {'dir1': 'down', 'dir2': 'right', 'push': 'top', 'class': 'stack-topleft'},
	'topbar': {'dir1': 'down', 'dir2': 'right', 'push': 'top', 'spacing1': 0, 'spacing2': 0, 'class': 'stack-bar-top'},
	'bottombar': {'dir1': 'up', 'dir2': 'right', 'spacing1': 0, 'spacing2': 0, 'class': 'stack-bar-bottom'}
};

var button_type = {
	'default': {'class': ''},
	'primary': {'class': 'btn-primary'},
	'warning': {'class': 'btn-warning'},
	'danger': {'class': 'btn-danger'},
	'success': {'class': 'btn-success'}
};

/* Generates a callback function for click event on notification's custom buttons. */
function callback_factory(callback_url, button_id)
{
	var f = function(notification)
	{
		/* Morph the notification to a loading animation. */
		notification.update(loading_notification);

		/* Perform a POST callback to the server. */
		var post_data = {
			prompt: notification.modules.confirm.prompt.val(),
			button: button_id
		};
		$.post(callback_url, post_data, function (data) {
			/* If the response contains a notification, display it... */
			if (data.data) {
				var options = get_pnotify_options(data.data);
				notification.update(options);
			}
			/* ...otherwise remove the loading animation. */
			else {
				notification.remove();
			}
		})
		.fail(function () {
			/* If callback fails, morph the notification to an error notification. */
			notification.update(server_error_notification);
		});
	};
	
	return f;
}

/* Translates notification data from backend to PNotify options object.
   Use it override option defaults (in fields that are not passed to the function).
*/
function get_pnotify_options(options)
{
	var __options__ = $.extend(true, {}, PNotify.prototype.options);

	/* Basic options */
	__options__.title = options.title ? options.title : __options__.title;
	__options__.text = options.text ? options.text : __options__.text;
	__options__.icon = options.icon ? options.icon : __options__.icon;
	__options__.type = options.type ? options.type : __options__.type;

	/* Stack option */
	__options__.stack = options.stack ? stacks[options.stack] : stacks['default'];
	__options__.addclass = options.stack ? stacks[options.stack]['class'] + ' ' : stacks['default']['class'] + ' ';

	/* Misc. */
	__options__.addclass += options.additional_class ? options.additional_class : '';
	__options__.hide = options.hide ? options.hide : __options__.hide;
	__options__.buttons.closer = options.hide_button ? options.hide_button : __options__.buttons.closer;
	__options__.buttons.sticker = false;

	/* Buttons */
	__options__.confirm.buttons = [];
	__options__.confirm.confirm = false;
	if (options.buttons instanceof Array) {
		$.each(options.buttons, function(k, button) {
			__options__.confirm.buttons.push({
				text: button.label,
				addClass: button_type[button.type]['class'],
				click: callback_factory(options.callback_url, button.id),
				promptTrigger: button.prompt_trigger,
			});
			__options__.confirm.confirm = true;
		});
	}

	/* Prompt */
	__options__.confirm.prompt = options.prompt != undefined ? options.prompt : false;
	__options__.confirm.prompt_multi_line = options.prompt_multi_line ? options.prompt_multi_line : false;
	__options__.confirm.prompt_default = options.prompt_default ? options.prompt_default : '';

    if (__options__.nonblock && options.nonblock) {
        __options__.nonblock.nonblock = options.nonblock.nonblock ? options.nonblock.nonblock : false;
        __options__.nonblock.nonblock_opacity = options.nonblock.nonblock_opacity ? options.nonblock.nonblock_opacity : .2
    }
	return __options__;
}

function create_notification(options)
{
	(new PNotify(options)).get()
	  .on('pnotify.confirm');
}

$(function () {
	/* Setup default stack */
	stacks['default'] = stacks['topright'];

	/* Generate notifications */
	loading_notification = get_pnotify_options({
		title: "Please wait...",
		text: '\
		<div class="progress progress-striped active">\
			<div class="progress-bar" role="progressbar" style="width: 100%">\
				<span class="sr-only">Waiting for server response.</span>\
			</div>\
		</div>\
		',
		hide: false,
	});
	
	server_error_notification = get_pnotify_options({
		title: "Error",
		text: 'Server error encountered. Please try again later.',
		type: 'error'
	});

	/* Testing */
	$.get('/ajax/messaging/', function (data) {
		$.each(data.data, function (k, notification){
			create_notification(get_pnotify_options(notification));
		});
	});
});