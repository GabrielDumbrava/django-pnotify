from django.conf.urls import patterns, url

urlpatterns = patterns('messaging.views',
    url(r'^$', 'get_notifications', name='get_notifications'),
    url(r'^(?P<pk>\d+)', 'notification_callback', name='notification_callback'),
)
