from jsonresponse import to_json
from django.contrib.auth.decorators import login_required
from django.contrib.messages import get_messages
from django.shortcuts import get_object_or_404
from .models import Message
from .notifications import FlashMessageNotification


@to_json('api')
def get_notifications(request):
    notifications = []

    if request.user.is_authenticated():
        # Process app messages
        messages = Message.objects.inbox(request.user)
        for message in messages:
            notification = message.notification()
            notifications.append(notification.serialize(request))
            notification.post_display()

        # Process django flash messages
        flash_messages = get_messages(request)
        for message in flash_messages:
            notifications.append(FlashMessageNotification(message).serialize(request))

    return notifications


@to_json('api')
@login_required
def notification_callback(request, pk):
    # Get message and notifictation
    message = get_object_or_404(Message, pk=pk, receiver=request.user)
    notification = message.notification()

    # Perform callback
    callback_notification = notification.callback(request)
    res = callback_notification.serialize(request) if hasattr(callback_notification, 'serialize') else None
    if hasattr(callback_notification, 'post_display'):
        callback_notification.post_display()

    return res